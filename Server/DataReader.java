import java.nio.channels.CompletionHandler;
import java.nio.ByteBuffer;

public interface DataReader {
    void beforeRead(Connection client);
    void onData(Connection client, ByteBuffer buffer, int bytes);
    boolean acceptsMessages();
}