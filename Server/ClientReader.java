import java.nio.channels.CompletionHandler;
import java.nio.ByteBuffer;

// Handles reading and writing to the Connection client
class ClientReader {

    private final DataReader callback;
    private final BattleZoneServer server;

    ClientReader(BattleZoneServer server, DataReader callback) {

        this.server = server;
        this.callback = callback;

    }

    public boolean acceptsMessages() {

        return callback.acceptsMessages();

    }

    // Runs cyclic processing of actions before read and queues up a new read. Handles closed and error'd channels. If client is still connected, a new round is processed.
    public void run(final Connection client) {

        callback.beforeRead(client);
        
        client.read(new CompletionHandler<Integer, ByteBuffer>() {

            @Override
            public void completed(Integer result, ByteBuffer buffer) {

                if (result < 1) { // connection has been closed or something gone wrong

                    client.close();
                    System.out.println("Closing connection to " + client);
                    server.removeConnection(client);

                } else {

                    callback.onData(client, buffer, result);
                    client.run();

                }
            }

            @Override
            public void failed(Throwable exc, ByteBuffer buffer) {

                client.close();
                server.removeConnection(client);

            }

        });

    }

}