import javafx.scene.Node;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

import java.util.Arrays;
import java.util.Random;
import java.lang.Math;

public class Tank {

    public Polygon sprite;
    private double rotationIncrement = 10; // increment of rotation in degrees
    private double movementIncrement = 10; // increment of forward/backward movement in pixels
    private boolean alive;
    final private static double width = 50; // sprite width
    final private static double height = 53.4; // sprite height
    private String username;

    public Tank(String username, double boundX, double boundY) {

        this.username = username;
        alive = true;

        sprite = new Polygon();

        // Set sprite size, rotation, and location
        sprite.getPoints().addAll(new Double[] {
            (width/2),0.0,
            0.0,height,
            width,height
        });

        // default pivot is formed as the center of an imaginary square. real center of this triangle is offset by 7 pixels
        readjustPivot(sprite,0,6); 
        
        // Move sprite to center
        sprite.setTranslateX(boundX/2);
        sprite.setTranslateY(boundY/2);
        
    }

    // this doesn't work. no clue why. would be nice. but we can't have nice things.
    public void setUsername(String username) {
        System.out.println("Set username to " + username);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public double getX() {
        return sprite.getTranslateX();
    }

    public double getY() {
        return sprite.getTranslateY();
    }

    public double getAngle() {
        return sprite.getRotate();
    }

    public boolean isAlive() {
        return alive;
    }

    public static int getDepth() {

        return (int)height;

    }

    public static int getWidth() {
        
        return (int)width;

    }

    // Tank calculates the trajectory of it's own new bullets
    public Bullet newBullet() {

        //System.out.println("Tank firing from " + sprite.getTranslateX() + "," + sprite.getTranslateY() + "@" + sprite.getRotate());
        return new Bullet(sprite.getTranslateX(), sprite.getTranslateY(), sprite.getRotate(), width, height);

    }

    private void readjustPivot(Node node, double offsetX, double offsetY) {

        node.getTransforms().add(new Translate(-offsetX,-offsetY));
        node.setTranslateX(offsetX);
        node.setTranslateY(offsetY);

    }

    public void rotateLeft() {

        if((sprite.getRotate() - rotationIncrement) < 0) {
            sprite.setRotate(360);
        }

        sprite.setRotate((sprite.getRotate() - rotationIncrement) % 360);
    }

    public void rotateRight() {

        sprite.setRotate((sprite.getRotate() + rotationIncrement) % 360);

    }

    public void moveForward() {

        double currentHeadingRads = Math.toRadians(sprite.getRotate());

        double dx = Math.sin(currentHeadingRads) * movementIncrement;
        double dy = Math.cos(currentHeadingRads) * movementIncrement * -1; // for some reason this is coming out negative of what's expected. no time to figure out why, just slap -1 on it ;)

        sprite.setTranslateX(sprite.getTranslateX() + dx);
        sprite.setTranslateY(sprite.getTranslateY() + dy);

    }

    public void moveBackward() {

        double currentHeadingRads = Math.toRadians(sprite.getRotate());

        double dx = Math.sin(currentHeadingRads) * movementIncrement * -1; // negative direction!
        double dy = Math.cos(currentHeadingRads) * movementIncrement;

        sprite.setTranslateX(sprite.getTranslateX() + dx);
        sprite.setTranslateY(sprite.getTranslateY() + dy);

    }

}