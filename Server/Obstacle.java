import javafx.scene.shape.Rectangle;
import java.util.Random;

public class Obstacle {
    public Rectangle rectangle;
    private double boundX, boundY;
    private Random rand;
    double x, y;
    final int width;
    final int depth; // not sure what to do with this for our top-down implementation

    public Obstacle(double boundX, double boundY) {

        width = 50;
        depth = 50;
        this.x = 0;
        this.y = 0;

        this.rand = new Random();

        this.boundX = boundX;
        this.boundY = boundY;

        // Randomize location
        while (x <= width || x >= this.boundX - width) {

            this.x = (int) (rand.nextDouble() * boundX);

        }

        while (y <= width || y >= this.boundY - width) {

            this.y = (int) (rand.nextDouble() * boundY);

        }

        this.rectangle = new Rectangle(x, y, width, width);
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getDepth() {
        return this.depth;
    }
}