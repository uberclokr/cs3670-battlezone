import java.nio.ByteBuffer;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

// Writes all messages in buffer to other clients
class MessageReader implements DataReader {

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");

    private final BattleZoneServer server;

    public MessageReader(BattleZoneServer server) {

        this.server = server;

    }

    public boolean acceptsMessages() {

        return true;

    }

    @Override
    public void beforeRead(Connection client) {

        // Check for messages
        String message = client.nextMessage();

        while (message != null) {

            try {

                Thread.sleep(25); // Don't eat the processor. Read from clients at same rate that server sends out updates

            } catch (InterruptedException ex) {

                return;

            }

            if(BattleZoneServer.DEBUG) {

                System.out.println(dtf.format(LocalDateTime.now()) + " RCVD " + message);
                
            }
            
            server.respondToClient(client, message);

            message = client.nextMessage();

        }

    }

    // Append read buffer to client message buffer
    //  client - client to append messages to
    //  buffer - buffer received from socket in question
    //  bytes - number of bytes read into said buffer
    @Override
    public void onData(Connection client, ByteBuffer buffer, int bytes) {

        buffer.flip();
        // Just append the message on the buffer
        client.appendMessage(new String(buffer.array(), 0, bytes));

    }

}