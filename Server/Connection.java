import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;
import java.io.IOException;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.*;
import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;
import java.nio.channels.AsynchronousSocketChannel;
import org.json.JSONObject;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

// Connection - representation of a remote connection to this server
class Connection {

    private final AsynchronousSocketChannel channel;
    private AtomicReference<ClientReader> reader;
    private String username;
    
    private final StringBuilder messageBuffer = new StringBuilder();
    private static final String NEWLINE = "\0";
    private final Queue<ByteBuffer> queue = new LinkedList<ByteBuffer>();
    private boolean writing = false;

    private boolean connected = false;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");

    public Connection(AsynchronousSocketChannel channel, ClientReader reader) {

        this.channel = channel;

        try {

            this.username = this.channel.getRemoteAddress().toString().replace("/", "");

        } catch (IOException ex) {

            ex.printStackTrace();

        }

        this.reader = new AtomicReference<ClientReader>(reader);

    }

    // Disconnect client
    public void disconnect() {

        try {

            this.channel.close();

        } catch (IOException ex) {

            System.err.println("Could not terminate client socket channel");

        }

    }

    public Channel getChannel() {
        return this.channel;
    }

    // Queue a write to the buffer
    private void writeMessage(final ByteBuffer buffer) {

        boolean threadShouldWrite = false;

        synchronized (queue) {

            queue.add(buffer);

            // Currently no thread writing, make this thread dispatch a write
            if (!writing) {

                writing = true;
                threadShouldWrite = true;

            }

        }

        if (threadShouldWrite) {

            writeFromQueue();

        }
    }

    private void writeFromQueue() {

        ByteBuffer buffer;

        synchronized (queue) {

            buffer = queue.poll();

            if (buffer == null) {

                writing = false;
            }

        }

        if (writing) {

            writeBuffer(buffer);

        }
    }

    private void writeBuffer(ByteBuffer buffer) {

        channel.write(buffer, buffer, new CompletionHandler<Integer, ByteBuffer>() {

            @Override
            public void completed(Integer result, ByteBuffer buffer) {

                if (buffer.hasRemaining()) {

                    channel.write(buffer, buffer, this);

                } else {

                    // Go back and check if there is new data to write
                    writeFromQueue();

                }

            }

            @Override
            public void failed(Throwable exc, ByteBuffer attachment) {

            }

        });

    }

    // Send message
    public void writeStringMessage(String string) {

        writeMessage(ByteBuffer.wrap(string.getBytes()));

    }

    // Send a message from a specific client
    public void writeMessage(String message) {

        if (reader.get().acceptsMessages()) {
            
            writeStringMessage(message);

        } else {

            System.out.println("Reader does not accept messages");

        }

    }

    // Read message. Call CompletionHandler on completion of message
    public void read(CompletionHandler<Integer, ? super ByteBuffer> completionHandler) {

        ByteBuffer input = ByteBuffer.allocate(128);

        if (!channel.isOpen()) {
            return;
        }

        channel.read(input, input, completionHandler);

    }

    public void setConnected() {
        connected = true;
    }

    public boolean isConnected() { 
        return connected;
    }

    // Close the channel
    public void close() {

        try {

            channel.close();

        } catch (IOException ex) {

            ex.printStackTrace();

        }

    }

    // Run current state's action
    public void run() {

        reader.get().run(this);

    }

    public void setReader(ClientReader reader) {

        this.reader.set(reader);

    }

    public void setUsername(String user) {

        this.username = user.trim();

    }

    public String getUsername() {

        return this.username;

    }

    public void appendMessage(String message) {

        synchronized (messageBuffer) {

            messageBuffer.append(message);

        }

    }

    // Get next newline-separated message on buffer. Return null if there's no newline.
    public String nextMessage() {

        synchronized (messageBuffer) {

            int nextNewline = messageBuffer.indexOf(NEWLINE);

            if (nextNewline == -1) {

                return null;

            }

            String message = messageBuffer.substring(0, nextNewline + 1);
            messageBuffer.delete(0, nextNewline + 1);
            return message;

        }

    }

}