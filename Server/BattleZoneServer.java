// Parker Hill
// UVID 10760574
// Updated: 2/28/2018

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;
import java.io.IOException;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.*;
import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;
import java.nio.channels.AsynchronousSocketChannel;
import org.json.JSONObject;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

// Multi-threaded, asynchronous chat server. Maintains a list of active connections with instances of 'Client' class.
public class BattleZoneServer implements Runnable {

    // NOTE - synchronized list allows safe access and iteration while it's members are operated on in multiple threads
    private final List<Connection> connections = Collections.synchronizedList(new ArrayList<Connection>());
    private int port;
    private final AsynchronousServerSocketChannel listener;
    private final AsynchronousChannelGroup channelGroup;
    protected BattleZoneGame game;

    protected final static boolean DEBUG = true;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");

    // port - server's listening port
    public BattleZoneServer(int port) throws IOException {

        this.channelGroup = AsynchronousChannelGroup.withFixedThreadPool(Runtime.getRuntime().availableProcessors(),
                Executors.defaultThreadFactory());
        this.port = port;
        this.listener = createListener(channelGroup);

    }

    // return local address of socket
    public SocketAddress getSocketAddress() throws IOException {

        return this.listener.getLocalAddress();

    }

    public void run() {

        game = new BattleZoneGame();

        // start waiting for connections, calls CompletionHandler
        listener.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {

            @Override
            public void completed(AsynchronousSocketChannel result, Void attachment) {

                // request a new accept and handle the incoming connection
                listener.accept(null, this);
                handleNewConnection(result);

            }

            @Override
            public void failed(Throwable exc, Void attachment) {
            }

        });

        
        int updateInterval = game.getUpdateInterval();

        while(true) {

            try {

                Thread.sleep(updateInterval);

            } catch (InterruptedException ex) {

                return;

            }

            update();                  
        }

    }

    // Safe server shutdown
    public void shutdown() throws InterruptedException, IOException {

        channelGroup.shutdownNow();
        channelGroup.awaitTermination(1, TimeUnit.SECONDS);

    }

    // Create listener and bind socket
    private AsynchronousServerSocketChannel createListener(AsynchronousChannelGroup channelGroup) throws IOException {

        final AsynchronousServerSocketChannel listener = openChannel(channelGroup);
        listener.setOption(StandardSocketOptions.SO_REUSEADDR, true);
        listener.bind(new InetSocketAddress(port));
        return listener;

    }

    // Open socket channel
    private AsynchronousServerSocketChannel openChannel(AsynchronousChannelGroup channelGroup) throws IOException {

        return AsynchronousServerSocketChannel.open(channelGroup);

    }

    // Create a new client, add it to the list
    private void handleNewConnection(AsynchronousSocketChannel channel) {

        Connection client = new Connection(channel, new ClientReader(this, new MessageReader(this)));
        connections.add(client);
        client.run();

    }

    public void update() {

        game.moveBullets(); // bullets move independently of player action and should be actioned here

        String message = BattleZoneNetProto.update(game.getTanks(), game.getBullets(), game.getMessageBuffer());

        if(BattleZoneServer.DEBUG) {
            System.out.println(dtf.format(LocalDateTime.now()) + " SENT " + message);
        }

        synchronized (connections) {

            // Write all queued messages to other clients - each client has their own worker thread, so this shouldn't be a problem...
            for (Connection clientConnection : connections) {

                clientConnection.writeMessage(message);

            }

        }

    }

    // Send a message to all clients
    public void respondToClient(Connection client, String message) {

        // Clean message
        message =  message.replace("\n", "").replace("\r", "");

        // Write message to console for debug
        //System.out.print(client.getUsername() + "> " + message);

        ////////////////////////////////////////////////////////////////
        // PROCESS RECEIVED MESSAGES - UPDATE GAME MODEL AS NECESSARY //
        ////////////////////////////////////////////////////////////////

        // If JSON
        if(message.charAt(0) == '{') {
        
            // Get command
            JSONObject json = new JSONObject(message);
            String[] command = JSONObject.getNames(json);

            // If command exists
            if(command.length > 0) {

                // Get command parameters
                JSONObject innerJson = json.getJSONObject(command[0]);

                /*
                MOVE
                {
                    "move": {
                        "username": string,
                        "direction": string     // FORWARD, BACKWARD, LEFT, RIGHT
                    }
                }
                */
                if(command[0].equals("move")) {

                    String username = innerJson.get("username").toString();
                    String direction = innerJson.get("direction").toString();

                    // Move player's tank
                    game.moveTank(username,direction);
                    return;
                }

                /*
                FIRE
                {
                    "fire": {
                        "username": string
                    }
                }
                */
                else if(command[0].equals("fire")) {

                    String username = innerJson.get("username").toString();

                    // Fire bullet from user's tank
                    game.fireBullet(username);
                    return;
                }

                /*
                CONNECT 
                {
                    "connect": {
                        "username": string
                    }
                }
                */
                else if(command[0].equals("connect")) {

                    String username = innerJson.getString("username").toString();

                    // get username - make sure the requested one is not already in use, keep appending 0's until an acceptable one is found
                    username = getNonduplicateUsername(username);

                    // add user to game model
                    game.addPlayer(username);

                    // update username associated with connection
                    client.setUsername(username);

                    // mark client as connected and ready to receive update messages
                    client.setConnected();

                    message = BattleZoneNetProto.connect(username, game.getBounds(), game.getObstacleArray(), Tank.getWidth(), Tank.getDepth());

                }

                /*
                DISCONNECT
                {
                    "disconnect": {
                        "username": string
                    }
                }
                */
                else if(command[0].equals("disconnect")) {

                    String username = innerJson.get("username").toString();

                    // remove player from game model
                    game.removePlayer(username);
                    
                    // terminate connection
                    synchronized (connections) {

                        client.disconnect();
                        connections.remove(client);

                    }
                    return;
                }

            }

        }

        synchronized (connections) {

            client.writeMessage(message);

        }

    }

    public String getNonduplicateUsername(String requestedUsername) {

        boolean duplicateExists = false;

        // Check if there's a duplicate among all connected users
        for (Connection clientConnection : connections) {

            if(clientConnection.getUsername().equals(requestedUsername)) {

                duplicateExists = true;

            }

        }

        if(duplicateExists) { // If duplicate exists, request username with 0 appended to it

            return getNonduplicateUsername(requestedUsername + "0");            

        } else { // else, give them requested username

            return requestedUsername;

        }

        

    }

    public void updateClients(String message) {
        synchronized (connections) {

            // Write all queued messages to other clients
            for (Connection client : connections) {

                if(client.isConnected()) {
                    client.writeMessage(message);
                }

            }

        }
    }

    // Remove client from the list
    public void removeConnection(Connection client) {

        connections.remove(client);

    }

    private static void usage() {

        System.err.println("BattleZoneServer [-p <port number>]");
        System.exit(1);

    }

    public static void main(String[] args) throws IOException {

        int port = 5000;
        if (args.length != 0 && args.length != 2) {

            usage();

        } else if (args.length == 2) {

            try {

                if (args[0].equals("-p")) {

                    port = Integer.parseInt(args[1]);

                } else {

                    usage();

                }

            } catch (NumberFormatException ex) {

                usage();

            }
        }

        System.out.println("BattleZoneServer started on port " + port);
        new BattleZoneServer(port).run();

    }

}