import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

/*
    BattleZoneGame: Represents a model of the game. 
    Contains coordinate models of all elements that clients should be replicating on their UI
    Parallels the 'BattleZonePane' class on the client
*/

public class BattleZoneGame {

    private int updateInterval = 50; // 50ms interval = 20 updates/sec
    private int obstacleCount = 4;
    private List<Tank> tanks;
    private List<Bullet> bullets;
    private List<Obstacle> obstacles;
    private final double boundX = 600; // client screen width
    private final double boundY = 600; // client screen height
    private String messageBuffer = "battle has begun";

    public BattleZoneGame() {

        tanks = Collections.synchronizedList(new ArrayList<Tank>());
        bullets = Collections.synchronizedList(new ArrayList<Bullet>());
        obstacles = Collections.synchronizedList(new ArrayList<Obstacle>());

        initialize();

    }

    public void addPlayer(String username) {

        synchronized (tanks) {

            tanks.add(new Tank(username,boundX,boundY));

        }

    }

    public void removePlayer(String username) {

        synchronized (tanks) {

            tanks.remove(getTankByUsername(username));

        }

    }

    public void setMessageBuffer(String message) {

        messageBuffer = message;

    }

    public String getMessageBuffer() {
        String message = messageBuffer;
        messageBuffer = "";
        return message;

    }

    public void moveBullets() {

        ArrayList<Bullet> toRemove = new ArrayList<Bullet>();

        synchronized (bullets) {

            double windowEdgeBuffer = 100;
            for(Bullet bullet : bullets) {

                bullet.move();    

                // If bullet goes out of bounds, delete it
                if((bullet.circle.getTranslateX() + windowEdgeBuffer) < 0 || (bullet.circle.getTranslateX() -  + windowEdgeBuffer) > boundX || (bullet.circle.getTranslateY() + windowEdgeBuffer) < 0 || (bullet.circle.getTranslateY()  - windowEdgeBuffer) > boundY) {
                    
                    toRemove.add(bullet);

                }

                // TODO - Account for bullet collision here (i.e. TANK DESTROYED)
            }

        }

        // Remove all bullets that have gone out of bounds or collided
        for(Bullet bullet : toRemove) {

            synchronized (bullets) {
                bullets.remove(bullet);
            }

        }

    }

    public void moveTank(String username, String direction) {

        Tank mover = getTankByUsername(username);

        switch(direction) {

            case "FORWARD":
                mover.moveForward();
                break;
            case "BACKWARD":
                mover.moveBackward();
                break;
            case "LEFT":
                mover.rotateLeft();
                break;
            case "RIGHT":
                mover.rotateRight();
                break;
            default:
                break;

        }

    }

    public List<Tank> getTanks() {

        return tanks;

    }

    public Obstacle[] getObstacleArray() {

        Obstacle[] obstacleArray = new Obstacle[obstacles.size()];

        int i = 0;
        for(Obstacle obstacle : obstacles) {
            obstacleArray[i++] = obstacle;
        }

        return obstacleArray;

    }

    public List<Obstacle> getObstacles() {
        return obstacles;
    }

    public List<Bullet> getBullets() {

        return bullets;

    }

    public double[] getBounds() {

        double[] bounds = {boundX,boundY};
        return bounds;

    }

    public int getUpdateInterval() {

        return updateInterval;

    }

    public void fireBullet(String username) {
        
        Tank shooter = getTankByUsername(username);

        synchronized(bullets) {

            bullets.add(shooter.newBullet());

        }

    }

    public void initialize() {

        // Setup obstacles
        for (int i = 0; i < obstacleCount; i++) {

            synchronized (obstacles) {

                obstacles.add(new Obstacle(boundX, boundY));

            }

        }

    }

    private Tank getTankByUsername(String username) {

        synchronized (tanks) {

            for(Tank tank : tanks) {

                if(tank.getUsername().equals(username)) {

                    return tank;

                }

            }

        }

        System.out.println("Could not find a tank! Game may be missing a player");

        return null;

    }

}