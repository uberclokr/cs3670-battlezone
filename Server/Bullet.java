import javafx.scene.shape.Circle;

import java.lang.Math;
import java.util.Random;

public class Bullet {
    
    public int id;
    public Circle circle;
    private final double radius = 3;
    private double boundX, boundY;
    private double speed = 15; // pixels per poll
    private double dx = 1, dy = 1;    
    private Random rand;

    // Spawn bullet with specific location, direction
    public Bullet(double x, double y, double angle, double width, double height) { // should probably just provide origin tank as argument here since it has all of these values in one object

        this.rand = new Random();

        this.id = this.rand.nextInt();

        double currentHeadingRads = Math.toRadians(angle);
        this.dx = Math.sin(currentHeadingRads) * speed;
        this.dy = Math.cos(currentHeadingRads) * speed * -1;

        this.circle = new Circle((x + (width/2)), (y + (height/2)), this.radius);

        this.circle.setTranslateX(x);
        this.circle.setTranslateY(y);
        this.circle.setRotate(angle);
    }

    public int getId() {

        return id;

    }

    public double getX() {

        return circle.getTranslateX();

    }

    public double getY() {

        return circle.getTranslateY();

    }

    public double getAngle() {
    
        return circle.getRotate();

    }

    public void move() {

        double currentHeadingRads = Math.toRadians(circle.getRotate());

        double dx = Math.sin(currentHeadingRads) * speed;
        double dy = Math.cos(currentHeadingRads) * speed * -1; // for some reason this is coming out negative of what's expected. no time to figure out why, just slap -1 on it ;)

        circle.setTranslateX(circle.getTranslateX() + dx);
        circle.setTranslateY(circle.getTranslateY() + dy);

        //System.out.println("Bullet " + id + " moving x+" + dx + " y+" + dy);

    }

    public void reverseX() {
        dx *= -1;
    }

    public void reverseY() {
        dy *= -1;
    }

    public void setId(int id) {

        this.id = id;

    }

}