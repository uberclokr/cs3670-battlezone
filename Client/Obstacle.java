import javafx.scene.shape.Rectangle;
import java.util.Random;

public class Obstacle {
    private Rectangle rectangle;

    // Place obstacle at specific point
    public Obstacle(double x, double y, int width, int depth) {

        this.rectangle = new Rectangle(x,y,width,depth);

    }

    public Rectangle getSprite() {
        return rectangle;
    }

    public double getX() {
        return this.rectangle.getTranslateX();
    }

    public double getY() {
        return this.rectangle.getTranslateY();
    }

    public int getWidth() {
        return (int) this.rectangle.getWidth();
    }

    public int getDepth() {
        return (int) this.rectangle.getHeight();
    }
}