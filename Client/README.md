# Build & Execute Instructions

## As .class file

        1. Make directory of Client source files your current folder
        cd /my/src/folder

        2. Compile .java source files (must be using JDK 1.8! verify with #java -version)
        javac BattleZoneClient.java

        3. Execute with Java
        java BattleZoneClient

## As JAR file
        1. Make directory of Client source files your current folder
        cd /my/src/folder
        
        2. Compile .java source files (must be using JDK 1.8! verify with #java -version)
        javac BattleZoneClient.java
        
        3. Compress to JAR
        jar cvfm BattleZoneClient.jar manifest.txt *.class
        
        4. Execute JAR!
        java -jar BattleZoneClient.jar
        
        Put this all into a simple hotkeyed script and you'll have a streamlined build & run process. In Visual Studio Code you can set this up under "Tasks>Run Build Task"

# Setting up Git
        1. Install git if you haven't already - https://git-scm.com/download/win
        
        2. Add repository
        git remote add origin https://<username>@bitbucket.org/uberclokr/cs3670-battlezone.git
        
        3. Change to desired local src directory
        cd /my/src/folder
        
        4. Do initial pull to get files (same command for subsequent pulls)
        git pull -u origin master
        
        For subsequent commits
        git commit -m "Comment for this commit"
        git push -u origin master
        
        Visual Studio Code also has handy buttons for issuing commits, pushes and pulls. They will function after the .git config has been created automatically after making your first pull

# Protocol:
        Java 8
        no frameworks, no Java RMI
        Client/Server architecture?

# Meta decisions
        What levevl of documentation?
        Tools used?
        Build/Deploy process
        
# Messages:

1.  Connect

    a.  Client

        i.  Connect Request

    b.  Server

        i.  Ack/Return Map

        ii. Deny

2.  Disconnect

    a.  Client

        i.  Disconnect Notification

    b.  Server

        i.  Ack?

        ii. Possible Disconnection Notification

3.  Action

    a.  Client

        i.  Move

            1.  Forward

            2.  Turn left

            3.  Turn Right

            4.  Backward

        ii. Fire

4.  Update

    a.  Server

        i.  Locations

            1. Tanks

            2. Bullets

        ii. Changes to Map

        iii. Tanks Destroyed

        iv. Win/Lose/Reset?