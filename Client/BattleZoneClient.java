import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class BattleZoneClient extends Application {

    private static String[] arguments;

    protected BattleZoneNetwork worker = null;
    protected ComboBox colorPicker;
    protected Stage stage;
    protected Scene scene;
    public BattleZonePane pane;

    protected final static boolean DEBUG = true;

    private String serverStr;
    private Integer port;
    private String username;

    public BattleZoneClient() {

        serverStr = "127.0.0.1";
        port = 5000;

    }

    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {

        stage = primaryStage;
        
        if(arguments.length > 0) { // User-defined server passed as command parameter

            // EXAMPLE: java BattleZoneClient user@255.255.255.255:5000
            if(arguments.length == 1) {
                // user
                username = arguments[0].split("@")[0];
                // 255.255.255.255
                serverStr = arguments[0].split("@")[1].split(":")[0];
                // 5000
                try {

                    port = Integer.parseInt(arguments[0].split(":")[1]);

                } catch (Exception ex) { port = 5000; }
            } else {
                System.out.println("Invalid argument: " + arguments[0]);
                Runtime.getRuntime().exit(0); 
            }



        } else { // Default server for testing

            username = "test";
            serverStr = "127.0.0.1";
            port = 5000;

        }

        // Initialize background worker and CONNECT TO SERVER
        try {

            // System.out.println("Connecting " + username + "@" + serverStr + ":" + port); // local debugging
            worker = new BattleZoneNetwork(this);
            worker.start();

            // Initialize player - send connect message to server
            sendMessage(BattleZoneNetProto.connect(username));            

        } catch (Exception ex) { Runtime.getRuntime().exit(0); } // Exit application

        // Generate stage components
        pane = new BattleZonePane(this); // Create game pane
        scene = new Scene(pane, pane.boundX, pane.boundY); // Create a scene and place it in the stage        

        // Initialize stage
        stage.setTitle("BattleZoneClient"); // Set the stage title
        stage.setScene(scene); // Place the scene in the stage
        stage.setResizable(false); // Prevent window from being resized to maintain easy consistency between networked peers
        stage.show(); // Display the stage
        pane.requestFocus(); // Must request focus after the primary stage is displayed
        pane.initialize();

        // Initialize event listeners
        pane.setOnKeyPressed(e -> { // Player movement handlers
            
            // For player actions here, don't move object on pane -- send request to server!
            if (e.getCode() == KeyCode.UP) {

                // pane.player.moveForward(); // moves player tank forward in local pane (NOT SERVER)
                worker.write(BattleZoneNetProto.move(username,"FORWARD"));

            } else if (e.getCode() == KeyCode.DOWN) {

                // pane.player.moveBackward();
                worker.write(BattleZoneNetProto.move(username,"BACKWARD"));

            } else if (e.getCode() == KeyCode.LEFT) {

                // pane.player.rotateLeft();
                worker.write(BattleZoneNetProto.move(username,"LEFT"));

            } else if (e.getCode() == KeyCode.RIGHT) {

                // pane.player.rotateRight();
                worker.write(BattleZoneNetProto.move(username,"RIGHT"));

            } else if (e.getCode() == KeyCode.SPACE) {

                worker.write(BattleZoneNetProto.fire(username));
                
            }
            
        });

        pane.setOnMouseClicked(e -> {

            System.out.println("Mouse " + e.getSceneX() + "," + e.getSceneY());

        });

        // Application closure handles
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() { // Handle program closure (CLI was previously still running before this was implemented)
            public void handle(WindowEvent e) {
                terminateNetworker();
            }
        });
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                sendMessage(BattleZoneNetProto.disconnect(username)); // send disconnect message to server on exit
                terminateNetworker();
                System.out.println("BattleZoneClient is exiting...");
            }
        });

    }

    public void terminateNetworker() {
        if(worker != null) {
            worker.interrupt(); // stop background thread
        }
    }

    public static void main(String[] args) {

        new BattleZoneClient();
        arguments = args;
        launch(args);
    
    }

    public String getFormattedMessage(String message) {

        return message; // no formatting

    }

    public String getServerStr() {

        return serverStr;

    }

    public Integer getServerPort() {

        return port;

    }

    public String getRequestedUsername() {
        return username;
    }

    public void updateUsername(String username) {

        this.username = username;

    }

    public void appendLog(String message) {

        //System.out.println(getFormattedMessage(message));

    }

    public void alertBox(String message) {

        Alert alert = new Alert(AlertType.ERROR);
        alert.setHeaderText(message);

        alert.showAndWait().ifPresent(rs -> {

            if (rs == ButtonType.OK) {

            }

        });

    }

    public void sendMessage(String message) {

        if(message.replace("\n","").replace("\r","").length() == 0) {

            // Zero length message - do nothing
            return;

        }   

        // CHECK FOR CLIENT COMMANDS

        // Default to writing simple text message
        if (worker != null) {

            message.replace("\n", "").replace("\r", "");

            worker.write(message);

        } else {

            System.out.println("Client is not connected! Connect to server first using: connect <username> <address> <port>");

        }

    }
}