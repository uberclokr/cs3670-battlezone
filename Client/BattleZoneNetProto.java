import java.util.ArrayList;
import java.util.List;

import javafx.scene.paint.Color;

import org.json.JSONObject;

// Definitions for "BattleZoneNetProto" - the networked bullet protocol
public class BattleZoneNetProto {

        // Server update response
        public static String update(List<Tank> tanks, List<Bullet> bullets, String message) {
    
            JSONObject innerJson = new JSONObject();

            synchronized(tanks) {
                innerJson.put("tanks",tanks.toArray());
            }

            synchronized(bullets) {
                innerJson.put("bullets",bullets.toArray());
            }

            innerJson.put("message",message);

            String json = new JSONObject().put("update", innerJson).toString();

            return json;

        }

        // Client connect request
        public static String connect(String username) {
            
            JSONObject innerJson = new JSONObject()
                .put("username", username);

            String json = new JSONObject()
                .put("connect", innerJson)
                .toString();

            return json;

        }
        
        // Client disconnect request
        public static String disconnect(String username) {
            JSONObject innerJson = new JSONObject()
                .put("username", username);

            String json = new JSONObject()
                .put("disconnect", innerJson)
                .toString();

            return json;
        }

        // Client move request
        public static String move(String username, String direction) {
            JSONObject innerJson = new JSONObject()
                .put("username", username)
                .put("direction", direction);

            String json = new JSONObject()
                .put("move", innerJson)
                .toString();

            return json;
        }

        // Client fire request
        public static String fire(String username) {
            JSONObject innerJson = new JSONObject()
                .put("username", username);

            String json = new JSONObject()
                .put("fire", innerJson)
                .toString();

            return json;
        }
}