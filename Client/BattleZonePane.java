import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.geometry.Bounds;
import javafx.application.Platform;

public class BattleZonePane extends Pane {

  public BattleZoneClient client;
  public static double boundX = 400;
  public static double boundY = 400;
  private Timeline animation;
  private double frameDuration = 50; // this should be less than or equal to the 'updateInterval' of the server's BattleZoneGame model
  //protected List<Bullet> bullets;
  protected Map<Integer, Bullet> bullets;
  protected Map<String, Tank> tanks;
  protected List<Obstacle> obstacles;
  //protected List<Tank> tanks;
  protected Tank player;
  private int id2;

  public BattleZonePane(BattleZoneClient client) {

    this.client = client;
    tanks = new ConcurrentHashMap<>(); //Collections.synchronizedList(new ArrayList<Tank>()); 
    bullets = new ConcurrentHashMap<>();
    obstacles = Collections.synchronizedList(new ArrayList<Obstacle>());

  }

  public void addObstacle(Obstacle obstacle) {

    synchronized (obstacles) {

      obstacles.add(obstacle);
      obstacle = obstacles.get(obstacles.size() -1);
      getChildren().add(obstacles.get(obstacles.size() - 1).getSprite());
      obstacle.getSprite().setFill(Color.BLACK);
      obstacle.getSprite().setFill(Color.TRANSPARENT);
      obstacle.getSprite().setStroke(Color.LIMEGREEN);
      obstacle.getSprite().setStrokeWidth(2);

    }

  }

  public void play() {

    animation.play();

  }

  public void pause() {

    animation.pause();

  }

  public void updateTank(String username, double x, double y, double angle, boolean alive) {

      if (tanks.get(username) != null) {
        //System.out.println("Moving tank " + username + " to " + x + "," + y + " @" + angle);
        synchronized (tanks) {
          tanks.get(username).changePosition(x, y, angle, alive);
        }
      } else {

        //System.out.println("Creating tank");
        synchronized (tanks) {
          tanks.put(username, new Tank(username, x, y, angle, alive));
          getChildren().add(tanks.get(username).getSprite());
        }
      }

      /*
    if(tanks.size() == 0) {
      //System.out.println("Creating tank");
      synchronized (tanks) {
        tanks.put(username, new Tank(username, x, y, angle, alive));
        getChildren().add(tanks.get(username).getSprite());
      }
    }
    */

  }

  public void updateBullet(int id, double x, double y, double angle) {

      if(bullets.get(id) != null) {
        synchronized (bullets) {
          bullets.get(id).updatePosition(x, y, angle);
          if(x < 0 || x > getWidth() || y < 0 || y > getWidth()) {
            System.out.println("Removing bullet at " + x + "," + y + " Bounds: " + getWidth() + "," + getHeight());
            getChildren().remove(bullets.get(id).getSprite());
            bullets.remove(id);

          }
        }
      }
      else {
        synchronized (bullets) {
          bullets.put(id, new Bullet(id, x, y, angle));
          getChildren().add(bullets.get(id).getSprite());
        }
      }

  }

  public static double getBoundsX() {

    return boundX;

  }

  public static double getBoundsY() {

    return boundY;

  }

  public static void setBoundsX(double width) {

    boundX = width;

  }

  public static void setBoundsY(double height) {

    boundY = height;

  }

  public Bullet getBulletByUsername(int id) {

    id2 = id;
	Bullet bullet = null;

    synchronized (bullets) {

    	  for(int id1 : bullets.keySet()) {
    	        Bullet checked = bullets.get(id1); {
    	          bullet = checked;
        }
      }

    }

    return bullet;

  }

  public int getTankCount() {

    int count = 0;

    synchronized (tanks) {
      count = tanks.size();
    }

    return count;
  }

  public void initialize() {

    setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));

    // Setup player
    player = new Tank(client.getRequestedUsername());

    //player.sprite.setFill(Color.BLUE);

    //getChildren().add(player.sprite);

    initializeAnimation();

    animation.setCycleCount(Timeline.INDEFINITE);
    animation.play(); // Start animation

  }

  public void initializeAnimation() {

    // Create an animation for moving bullets and player
    animation = new Timeline(new KeyFrame(Duration.millis(frameDuration), new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent e) {

        processPlayerMovement();
        processBulletMovement();

      }

    }));

  }

  public void processPlayerMovement() {

    // Once I can figure out how to set flags on key states (key down and key up), I'll call player movement functions here - Parker
    // This will make movement smoother rather than relying on system keyboard repeat rate

  }

  public void processBulletMovement() {

    ArrayList<Bullet> toRemove = new ArrayList<Bullet>();

    int index = 0;
    String removed = "Sending " + toRemove.size() + " bullets";

    synchronized (bullets) {

      for (Bullet bullet : toRemove) {

        index++;

        /* client.worker.write(BattleZoneNetProto.sendBullet(bullet),client.encrypt,client.digest,client.digestType);*/
        getChildren().remove(bullet.circle);

        bullets.remove(bullet);

      }

    }

    if (index > 0) {
      System.out.println(removed);
    }

  }

}