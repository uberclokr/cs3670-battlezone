import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.Future;
import java.lang.Double;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.Arrays;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import javafx.application.Platform;
import java.lang.Runnable;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONString;
import org.json.JSONException;

public class BattleZoneNetwork extends Thread {

    private BattleZoneClient client;
    private SocketAddress serverAddr;
    private AsynchronousSocketChannel channel;
    private Future result;
    private int hostId = 0;
    private boolean connected = false;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");

    BattleZoneNetwork(BattleZoneClient client) {
        this.client = client;
        serverAddr = new InetSocketAddress(this.client.getServerStr(), this.client.getServerPort());

        try {

            channel = AsynchronousSocketChannel.open();
            result = channel.connect(serverAddr);
            result.get();
            //this.client.appendLog("Connected to " + this.client.getServerStr());
            this.hostId = this.getLocalId();

        } catch (Exception ex) { // Needs refactoring

            this.client.alertBox(
                    "Could not connect to server at " + this.client.getServerStr() + ":" + this.client.getServerPort());
            return; // exit network worker thread since connection could not be established

        }

    }

    @Override
    public void run() {

        // "Background Worker Thread Started"

        try {

            while ((channel != null) && (channel.isOpen())) {

                ByteBuffer buffer = ByteBuffer.allocate(20480);
                Future result = channel.read(buffer);

                while (!result.isDone()) { // Wait for message completion

                }
                
                /*
                try {

                    Thread.sleep(25); // Take some time to breath... don't eat the processor

                } catch (InterruptedException ex) {

                    return;

                }*/

                buffer.flip(); // flip receive message before reading
                String message = new String(buffer.array()).trim();

                // Process message according to protocol before taking further action
                //System.out.println("RAW " + message); // debug- print raw message received
                message = processServerCommands(message);

                //String[] delimited = message.split(" ");

                if (Thread.currentThread().isInterrupted()) {

                    channel.close();
                    break; // Interrupt should be received by parent when program is closed

                }

            }

        } catch (Exception ex) {
            /* do nothing */ }

    }

    public String processServerCommands(String message) {

        if(BattleZoneClient.DEBUG) {
            System.out.println(dtf.format(LocalDateTime.now()) + " RCVD " + message);
        }

        ///////////////////////////////
        // PROCESS RECEIVED MESSAGES //
        ///////////////////////////////

        // If JSON
        if (message.charAt(0) == '{') {

            // Get command
            JSONObject json = new JSONObject(message);

            String[] command = JSONObject.getNames(json);

            // If command exists
            if (command.length > 0) {

                // Get command parameters
                JSONObject innerJson = json.getJSONObject(command[0]);

                if (command[0].equals("update") && isConnected()) {

                    // UPDATE
                    /* 
                    {
                        "update": {
                            "tanks": [{
                                "username": string,
                                "x": double,
                                "y": double,
                                "angle": double,
                                "alive": boolean
                            }],
                            "bullets": [{
                                "id": int,
                                "x": double,
                                "y": double,
                                "angle": double
                            }],
                            "message": string
                        }
                    }
                    */

                    String serverMessage = innerJson.getString("message");
                    if (serverMessage.length() > 0) {
                        System.out.println("server> " + serverMessage);
                    }

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            try {

                                JSONArray tanks = new JSONArray(innerJson.get("tanks").toString());

                                for (int i = 0; i < tanks.length(); i++) {
            
                                    JSONObject tank = tanks.getJSONObject(i);
                                    String username = tank.getString("username");
                                    double x = tank.getDouble("x");
                                    double y = tank.getDouble("y");
                                    double angle = tank.getDouble("angle");
                                    boolean alive = tank.getBoolean("alive");
            
                                    client.pane.updateTank(username, x, y, angle, alive);

                                    /*
                                    if(client.pane.getTankByUsername(username) != null) {
                                
                                        synchronized(client.pane.tanks) {
                                            
                                            client.pane.updateTank(username, x, y, angle, alive);
                                
                                        }
                                
                                    } else {
                                
                                        synchronized(client.pane.bullets) {
                                            // int id, double x, double y, double rot, double width, double height
                                            client.pane.bullets.add(new Bullet(id,x,y,angle));
                                
                                        }
                                
                                    }*/
            
                                }


                            } catch (Exception ex) {

                                ex.printStackTrace();

                            }
                        }
                    });


                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            try {

                                JSONArray bullets = new JSONArray(innerJson.get("bullets").toString());

                                for(int i = 0; i < bullets.length(); i++) {
                    
                                    JSONObject bullet = bullets.getJSONObject(i);
                                    int id = bullet.getInt("id");
                                    double x = bullet.getDouble("x");
                                    double y = bullet.getDouble("y");
                                    double angle = bullet.getDouble("angle");

                                    client.pane.updateBullet(id, x, y, angle);

                                }

                            } catch (Exception ex) {

                                ex.printStackTrace();

                            }

                        }
                    });
                

                }

                else if (command[0].equals("connect")) {

                    // CONNECT (response)
                    /*
                    {
                    "connect": {
                        "username": string,
                        "mapsize": [double,double],
                        "obstacles": [{
                            "x": double,
                            "y": double,
                            "width": int,
                            "depth": int
                        }]
                    }
                    }
                    */

                    String username = innerJson.get("username").toString();

                    // update this player's username with what has been accepted by the server
                    this.client.updateUsername(username);

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                // Resize map/stage
                                JSONArray mapsizeJson = new JSONArray(innerJson.get("mapsize").toString());
                                double[] mapsize = { mapsizeJson.getDouble(0), mapsizeJson.getDouble(1) };
                                client.stage.setWidth(mapsize[0]);
                                client.stage.setHeight(mapsize[1]);
                                client.pane.resize(mapsize[0], mapsize[1]);

                                // store and render obstacles
                                JSONArray obstacles = new JSONArray(innerJson.get("obstacles").toString());
                                for (int i = 0; i < obstacles.length(); i++) {
                                    JSONObject obstacle = obstacles.getJSONObject(i);
                                    double x, y;
                                    int width, depth;
                                    try {

                                        x = Double.parseDouble(obstacle.get("x").toString());
                                        y = Double.parseDouble(obstacle.get("y").toString());
                                        width = Integer.parseInt(obstacle.get("width").toString());
                                        depth = Integer.parseInt(obstacle.get("depth").toString());

                                        client.pane.addObstacle(new Obstacle(x, y, width, depth));

                                    } catch (Exception ex) {
                                        // do nothing
                                        System.out.println(ex.getMessage());
                                    }

                                }

                                // set global tank width and height
                                Tank.width = innerJson.getInt("tankwidth");
                                Tank.height = innerJson.getInt("tankdepth");

                            } catch (Exception ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    });

                    connected = true;

                }
            }
        }

        return message;
    }

    public boolean isConnected() {
        return connected;
    }

    // Write message to server
    public void write(String message) {
        if (message != null) {

            // Log all sent messages
            if(BattleZoneClient.DEBUG) {
                System.out.println(dtf.format(LocalDateTime.now()) + " SENT " + message);
            }

            ByteBuffer buffer;
            buffer = ByteBuffer.wrap((message + "\0").getBytes());
            result = channel.write(buffer);

            while (!result.isDone()) {
                // Wait
            }

            // this.client.appendLog("Me: " + message); // for local debugging
            // System.out.println(message);

        }
    }

    // Read messages from server
    public void read(CompletionHandler<Integer, ? super ByteBuffer> completionHandler) {

        ByteBuffer input = ByteBuffer.allocate(128);

        if (!channel.isOpen()) {

            return;

        }

        channel.read(input, input, completionHandler);

    }

    public int getLocalId() {

        if (hostId != 0) {

            return this.hostId;

        }

        try {

            this.hostId = Integer
                    .parseInt(channel.getLocalAddress().toString().replace("/", "").replace(".", "").replace(":", ""));

        } catch (IOException e) {

            // Do nothing

        }

        return this.hostId;

    }

}