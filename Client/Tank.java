import javafx.scene.Node;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.Random;
import java.lang.Math;

import javafx.application.Platform;

public class Tank {

    public Polygon sprite = new Polygon();
    private double rotationIncrement = 10; // increment of rotation in degrees
    private double movementIncrement = 10; // increment of forward/backward movement in pixels
    private boolean alive = true;
    public static double width = 50; // sprite width
    public static double height = 53.4; // sprite height
    private String username;
    private double x = 0, y = 0, angle = 0;

    // Create new tank at default location
    public Tank(String username) {

        this.username = username;

        sprite.getPoints().addAll(tankShape());
        sprite.setUserData(this.username);
        
        sprite.setFill(Color.TRANSPARENT);
        sprite.setStroke(Color.LIMEGREEN);
        sprite.setStrokeWidth(2);

        updateLocation();

    }

    // Create null tank
    public Tank() {
        this.username = "null";
    }

    // Create new tank at specific location and heading
    public Tank(String username, double x, double y, double angle, boolean alive) {

        this.username = username;
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.alive = alive;

        // Set sprite shape
        sprite.getPoints().addAll(tankShape());

        sprite.setFill(Color.TRANSPARENT);
        sprite.setStroke(Color.LIMEGREEN);
        sprite.setStrokeWidth(2);

        // default pivot is formed as the center of an imaginary square. real center of this triangle is offset by 7 pixels
        readjustPivot(sprite, 0, 6);

        // Move sprite and rotate accordingly
        updateLocation();

    }

    private void updateLocation() {
        
        sprite.setTranslateX(this.x);
        sprite.setTranslateY(this.y);
        sprite.setRotate(this.angle);

    }

    private Double[] tankShape() {
        return new Double[] {

                (width / 2), 0.0, 0.0, height, width, height

        };
    }

    public Polygon getSprite() {

        return sprite;

    }

    public static double getHeight() {
        return height;
    }

    public static double getWidth() {
        return width;
    }

    public void changePosition(double x, double y, double angle, boolean alive) {

        this.x = x;
        this.y = y;
        this.angle = angle;
        this.alive = alive;

        // move sprite and rotate accordingly
        updateLocation();

    }

    public void updatePosition(Tank newTank) {

        this.x = newTank.getX();
        this.y = newTank.getY();
        this.angle = newTank.getAngle();
        this.alive = newTank.isAlive();

        updateLocation();

    }

    public boolean setUsername(String username) {
        System.out.println("Set username to " + username);
        this.username = username;
        return true;
    }

    public String getUsername() {
        return username;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getAngle() {
        return angle;
    }

    public boolean isAlive() {
        return alive;
    }

    // Tank calculates the trajectory of it's own new bullets
    public Bullet newBullet() {

        System.out.println("Tank firing from " + sprite.getTranslateX() + "," + sprite.getTranslateY() + "@" + sprite.getRotate());
        return new Bullet(sprite.getTranslateX(), sprite.getTranslateY(), sprite.getRotate(), width, height);

    }

    private void readjustPivot(Node node, double offsetX, double offsetY) {

        node.getTransforms().add(new Translate(-offsetX, -offsetY));
        node.setTranslateX(offsetX);
        node.setTranslateY(offsetY);

    }

    public void rotateLeft() {

        if ((sprite.getRotate() - rotationIncrement) < 0) {
            sprite.setRotate(360);
        }

        sprite.setRotate((sprite.getRotate() - rotationIncrement) % 360);

    }

    public void rotateRight() {

        sprite.setRotate((sprite.getRotate() + rotationIncrement) % 360);

    }

    public void moveForward() {

        double currentHeadingRads = Math.toRadians(sprite.getRotate());

        double dx = Math.sin(currentHeadingRads) * movementIncrement;
        double dy = Math.cos(currentHeadingRads) * movementIncrement * -1; // for some reason this is coming out negative of what's expected. no time to figure out why, just slap -1 on it ;)

        sprite.setTranslateX(sprite.getTranslateX() + dx);
        sprite.setTranslateY(sprite.getTranslateY() + dy);

    }

    public void moveBackward() {

        double currentHeadingRads = Math.toRadians(sprite.getRotate());

        double dx = Math.sin(currentHeadingRads) * movementIncrement * -1; // negative direction!
        double dy = Math.cos(currentHeadingRads) * movementIncrement;

        sprite.setTranslateX(sprite.getTranslateX() + dx);
        sprite.setTranslateY(sprite.getTranslateY() + dy);

    }

}