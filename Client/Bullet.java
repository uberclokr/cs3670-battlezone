import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;

import java.lang.Math;
import java.util.Random;

public class Bullet {
    
    public int id;
    public Circle circle;
    private final double radius = 3;
    private double boundX, boundY;
    private double x = 0, y = 0;
    private double speed = 3; // pixels per poll
    private double dx = 1 * speed, dy = 1 * speed;
    private Random rand;
    private boolean local;
    private double angle;

    // Spawn bullet with specific location, direction
    public Bullet(int id, double x, double y, double rot) {

        this.x = x + (Tank.width/2);
        this.y = y + (Tank.height/2);

        double currentHeadingRads = Math.toRadians(rot);
        this.dx = Math.sin(currentHeadingRads) * speed;
        this.dy = Math.cos(currentHeadingRads) * speed * -1;

        this.circle = new Circle(this.x, this.y, this.radius);

        this.circle.setFill(Color.RED);

    }

    // Spawn bullet with specific location, scalar vector (dx/dy), and bounds
    public Bullet(double x, double y, double dx, double dy, double boundX, double boundY) {

        this.rand = new Random();
        this.id = this.rand.nextInt(10000);
        this.boundX = boundX;
        this.boundY = boundY;

        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;

        this.circle = new Circle(this.x, this.y, this.radius);

        this.circle.setFill(Color.RED);
    }

    public Bullet(double x, double y, double angle, double boundX, double boundY) {

        this.rand = new Random();
        this.id = this.rand.nextInt(10000);
        this.boundX = boundX;
        this.boundY = boundY;

        this.x = x;
        this.y = y;

        this.angle = angle;

        this.circle = new Circle(this.x, this.y, this.radius);

        this.circle.setFill(Color.RED);
    }

    public void changePosition(double x, double y, double angle) {

        this.x = x;
        this.y = y;
        this.angle = angle;

    }

    public Circle getSprite() {
        return this.circle;
    }

    public int getId() {

        return id;

    }

    public double getX() {

        return x;

    }

    public double getY() {

        return y;

    }

    public double getDx() {
        return dx;
    }

    public double getDy() {
        return dy;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
    
        return angle;

    }

    public void updatePosition(double x, double y, double angle) {
        this.x = x;
        this.y = y;
        this.angle = angle;

        this.circle.setCenterX(this.x);
        this.circle.setCenterY(this.y);
    }

    public void reverseX() {
        dx *= -1;
    }

    public void reverseY() {
        dy *= -1;
    }

    public void setLocal() {

        local = true;

    }

    public void setId(int id) {

        this.id = id;

    }

    public boolean isLocal() {

        return local;

    }

}